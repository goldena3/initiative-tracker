package Tracker;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

class Party{
	int partySize;
	JFrame frame;
	public Player[] players;
	
	public Party(int size, JFrame frame) {
		partySize = size;
		this.frame = frame;
	}
	
	public void buildParty() {
		Tracker.clearScreen();
		players = new Player[partySize];
		for(int i = 0; i < partySize; i++) {
			players[i] = new Player();
		}
		DarwAddPlayersScreen();
	}
	
	private void DarwAddPlayersScreen() {
		final JTextField[] fields = new JTextField[partySize];
		JPanel labelPanel = new JPanel(new GridLayout(0, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(0, 1));
		for(int i = 0; i < partySize; i++) {
			labelPanel.add(new JLabel("Player " + (i + 1)));
			fields[i] = new JTextField();
			fieldPanel.add(fields[i]);
		}
		JButton createPlayers = new JButton("Create Players");
		createPlayers.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				boolean AllNamesPresent = true;
				for(int i = 0; i < partySize && AllNamesPresent; i++) {
					AllNamesPresent = AllNamesPresent && !"".equals(fields[i].getText());
				}
				if(AllNamesPresent) {
					int i = 0;
					for(Player player: players) {
						player.name = fields[i].getText();
						i++;
					}
					Tracker.PrepCombat();
				}else {
					System.out.println(AllNamesPresent);
				}
			}
		});
		frame.add(labelPanel, BorderLayout.WEST);
	    frame.add(fieldPanel, BorderLayout.CENTER);
	    frame.add(createPlayers, BorderLayout.SOUTH);
	    Tracker.drawScreen();
	}

	
	
	
}