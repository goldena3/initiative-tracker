package Tracker;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Combat {
	List<Character> characters = new ArrayList<Character>();
	JFrame frame;
	public Combat(Party party, JFrame f) {
		frame = f;
		for(Player player: party.players) {
			characters.add(player);
		}
	}

	public void setInititiveScreen(int badGuys) {
		BadGuy BBEG = new BadGuy();
		BBEG.name = "Big Bad Guy";
		characters.add(BBEG);
		if(badGuys == 2) {
			BadGuy minions = new BadGuy();
			minions.name = "Minnions";
			characters.add(minions);
		}
		final JTextField[] inititivesInput = new JTextField[characters.size()];
		JPanel labelPanel = new JPanel(new GridLayout(0, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(0, 1));
		int i = 0;
		for(Character character: characters) {
			labelPanel.add(new JLabel(character.name));
			inititivesInput[i] = new JTextField(1);
			fieldPanel.add(inititivesInput[i]);
			i++;
		}
		
		JButton startCombat = new JButton("Start Combat");
		startCombat.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if(checkInputs()) {
					for(int i = 0; i < characters.size(); i++) {
						characters.get(i).inititive = Integer.parseInt(inititivesInput[i].getText());
					}
					characters.sort(new Comparator<Character>() {
							@Override
							public int compare(Character c1, Character c2) {
								return c2.inititive - c1.inititive;
							}
					});
					Tracker.clearScreen();
					runCombat();
				}
				
			}

			private boolean checkInputs() {
				for(JTextField input: inititivesInput) {
					try {
						Integer.parseInt(input.getText());
					}catch(Exception e) {
						return false;
					}
				}
				return true;
			}
			
		});
		frame.add(labelPanel, BorderLayout.WEST);
	    frame.add(fieldPanel, BorderLayout.CENTER);
	    frame.add(startCombat, BorderLayout.SOUTH);
		Tracker.drawScreen();
	}
	
	protected void runCombat() {
		int[] currentTurn = {0};
		JPanel labelPanel = new JPanel(new GridLayout(0, 1));
		JButton nextButton = new JButton("Next");
		JLabel[] labels = new JLabel[characters.size()];
		nextButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				labels[currentTurn[0]].setBorder(BorderFactory.createLineBorder(Color.BLACK, 5));
				currentTurn[0]++;
				if(currentTurn[0] == characters.size()) {
					currentTurn[0] = 0;
				}
				labels[currentTurn[0]].setBorder(BorderFactory.createLineBorder(Color.RED, 5));
			}
			
		});
		frame.add(nextButton, BorderLayout.NORTH);
		for(int i =0; i < characters.size(); i++) {
			labels[i] = new JLabel(characters.get(i).name);
			labels[i].setBorder(BorderFactory.createLineBorder(Color.BLACK, 5) );
			labelPanel.add(labels[i]);
		}
		JButton endCombat = new JButton("End Combat");
		labels[0].setBorder(BorderFactory.createLineBorder(Color.RED, 5));
		endCombat.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Tracker.PrepCombat();
			}
			
		});
		frame.add(labelPanel, BorderLayout.CENTER);
		frame.add(endCombat, BorderLayout.SOUTH);
		Tracker.drawScreen();
		
	}
}
