package Tracker;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Tracker {
	private static JFrame frame;
	private ArrayList<Player> players = new ArrayList<Player>();
	static Party party;
	public static void main(String[] args) {
		frame = new JFrame("Traker"); 
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	    frame.setLocationRelativeTo(null);
		AddplayersScreen();
	}
	
	static void drawScreen(){
		frame.pack(); 
	    frame.setVisible(true); 
	}
	
	public static void clearScreen() {
		frame.getContentPane().removeAll();
	}

	private static void AddplayersScreen() {		
	    JPanel labelPanel = new JPanel(new GridLayout(0, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(0, 1));
	    labelPanel.add(new JLabel("NO. Players"));
	    final JTextField playerInsert = new JTextField();
	    fieldPanel.add(playerInsert);
	    JButton createPartyButton = new JButton("Create Party");
	    createPartyButton.addActionListener(new ActionListener() {
	    	
	    		public void actionPerformed(ActionEvent e) {
	    			int numberOfPlayers;
	    			try{
	    				numberOfPlayers = Integer.parseInt(playerInsert.getText());
	    				if(numberOfPlayers > 0) {
	    					party = new Party(numberOfPlayers, frame);
	    					party.buildParty();
	    				}else{
	    					throw new Exception(numberOfPlayers + " is too few players");
	    				}
	    			}catch(Exception e1) {
	    				System.out.println(e1);
	    			}
	    		}
	    });
	    frame.add(labelPanel, BorderLayout.WEST);
	    frame.add(fieldPanel, BorderLayout.CENTER);
	    frame.add(createPartyButton, BorderLayout.SOUTH);
	    drawScreen();
	}

	public static void PrepCombat() {
	 clearScreen();
	 JPanel fieldPanel = new JPanel(new GridLayout(0, 1));
	 ButtonGroup combatTypes = new ButtonGroup();
	 final JRadioButton bigBadFight = new JRadioButton();	
	 bigBadFight.setText("Just big bad");
	 bigBadFight.setSelected(true);
	 JRadioButton BigBadWithMinnions = new JRadioButton();
	 BigBadWithMinnions.setText("Baddy with minions");
	 combatTypes.add(bigBadFight);
	 combatTypes.add(BigBadWithMinnions);
	 fieldPanel.add(bigBadFight);
	 fieldPanel.add(BigBadWithMinnions);
	 JButton setInititives = new JButton("Set Inititives");
	 setInititives.addActionListener(new ActionListener() {

		public void actionPerformed(ActionEvent e) {
			clearScreen();
			Combat combat = new Combat(party, frame);
			combat.setInititiveScreen(bigBadFight.isSelected() ? 1 : 2);
		}

	 });
	 frame.add(fieldPanel, BorderLayout.NORTH);
	 frame.add(setInititives, BorderLayout.SOUTH);
	 drawScreen();
	}

	
	 

}
